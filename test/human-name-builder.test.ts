import { expect } from 'chai'
import { HumanNameBuilder } from '../src/human-name.builder'

describe('human-name-builder', () => {
    let humanNameBuilder: HumanNameBuilder

    beforeEach(() => {
        humanNameBuilder = new HumanNameBuilder()
    })

    it('can set use to valid code', () => {
        humanNameBuilder.setUse('usual')
        expect(humanNameBuilder.build().use).to.equal('usual')
        humanNameBuilder.setUse('official')
        expect(humanNameBuilder.build().use).to.equal('official')
        humanNameBuilder.setUse('temp')
        expect(humanNameBuilder.build().use).to.equal('temp')
        humanNameBuilder.setUse('nickname')
        expect(humanNameBuilder.build().use).to.equal('nickname')
        humanNameBuilder.setUse('anonymous')
        expect(humanNameBuilder.build().use).to.equal('anonymous')
        humanNameBuilder.setUse('old')
        expect(humanNameBuilder.build().use).to.equal('old')
        humanNameBuilder.setUse('maiden')
        expect(humanNameBuilder.build().use).to.equal('maiden')
    })
    it('can not set invalid use', () => {
        expect(() => humanNameBuilder.setUse('INVALID')).to.throw()
    })
    it('can remove given name correctly', () =>{
        humanNameBuilder.addGivenName('a')
        humanNameBuilder.addGivenName('b')
        humanNameBuilder.addGivenName('c')
        humanNameBuilder.addGivenName('d')
        humanNameBuilder.addGivenName('e')

        expect(humanNameBuilder.removeGivenName('c')).to.equal(true)
        expect(humanNameBuilder.build().given).to.contain('a')
        expect(humanNameBuilder.build().given).to.contain('b')
        expect(humanNameBuilder.build().given).to.contain('d')
        expect(humanNameBuilder.build().given).to.contain('e')
    })
    
    it('can remove prefix correctly', () =>{
        humanNameBuilder.addPrefix('a')
        humanNameBuilder.addPrefix('b')
        humanNameBuilder.addPrefix('c')
        humanNameBuilder.addPrefix('d')
        humanNameBuilder.addPrefix('e')

        expect(humanNameBuilder.removePrefix('c')).to.equal(true)
        expect(humanNameBuilder.build().prefix).to.contain('a')
        expect(humanNameBuilder.build().prefix).to.contain('b')
        expect(humanNameBuilder.build().prefix).to.contain('d')
        expect(humanNameBuilder.build().prefix).to.contain('e')
    })
    
    
    it('can remove suffix correctly', () =>{
        humanNameBuilder.addSuffix('a')
        humanNameBuilder.addSuffix('b')
        humanNameBuilder.addSuffix('c')
        humanNameBuilder.addSuffix('d')
        humanNameBuilder.addSuffix('e')

        expect(humanNameBuilder.removeSuffix('c')).to.equal(true)
        expect(humanNameBuilder.build().suffix).to.contain('a')
        expect(humanNameBuilder.build().suffix).to.contain('b')
        expect(humanNameBuilder.build().suffix).to.contain('d')
        expect(humanNameBuilder.build().suffix).to.contain('e')
    })
    
    
})