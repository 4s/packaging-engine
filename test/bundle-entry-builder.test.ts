import { expect } from 'chai'
import { BundleEntryBuilder } from '../src/bundle-entry-builder'
import { BundleEntryRequest } from 'fhir'

describe('bundle-entry-builder', () => {
  let bundleEntryBuilder: BundleEntryBuilder

  beforeEach(() => {
    bundleEntryBuilder = new BundleEntryBuilder()
  })

  it('can set Resource', () => {
    bundleEntryBuilder.setResource({
      resourceType: 'Patient',
      identifier: [
        {
          system: 'urn:ietf:rfc:3986',
          value: 'value',
        },
      ],
    })

    expect(bundleEntryBuilder.build().resource).to.deep.equal({
      resourceType: 'Patient',
      identifier: [
        {
          system: 'urn:ietf:rfc:3986',
          value: 'value',
        },
      ],
    })
  })

  it('can not set a Resource that has no id, if the request method is GET', () => {
    bundleEntryBuilder.setMethod('GET')
    expect(() =>
      bundleEntryBuilder.setResource({
        resourceType: 'Patient',
        identifier: {
          system: 'urn:ietf:rfc:3986',
          value: 'value',
        },
        status: 'completed',
      })
    ).to.throw()
  })

  it('can not set resources which does not have a valid resourceType', () => {
    expect(() =>
      bundleEntryBuilder.setResource({
        resourceType: 'Invalid',
      })
    ).to.throw()
  })

  it('can not set method to DELETE if no resource is set', () => {
    expect(() => bundleEntryBuilder.setMethod('DELETE')).to.throw()
  })
  it('can not set method to PUT if no resource is set', () => {
    expect(() => bundleEntryBuilder.setMethod('PUT')).to.throw()
  })
  it('can not set method to DELETE if no resource.id is set', () => {
    bundleEntryBuilder.setResource({
      resourceType: 'Patient',
      identifier: {
        system: 'urn:ietf:rfc:3986',
        value: 'value',
      },
      status: 'completed',
    })

    expect(() => bundleEntryBuilder.setMethod('DELETE')).to.throw()
  })
  it('can not set method to PUT if no resource.id is set', () => {
    bundleEntryBuilder.setResource({
      resourceType: 'Patient',
      identifier: {
        system: 'urn:ietf:rfc:3986',
        value: 'value',
      },
      status: 'completed',
    })

    expect(() => bundleEntryBuilder.setMethod('PUT')).to.throw()
  })
  it('can not set request condition for invalid condition', () => {
    expect(() =>
      bundleEntryBuilder.setRequestCondition('invalid', 'invalid')
    ).to.throw()
  })
  it('can set request condition for valid condition and value', () => {
    bundleEntryBuilder.setRequestCondition('ifNoneMatch', 'value')
    bundleEntryBuilder.setMethod('GET')
    const entry = bundleEntryBuilder.build()

    expect(entry.request).to.deep.equal({ ifNoneMatch: 'value', method: 'GET' })
  })
  it('can set request condition for ifMatch with valid value', () => {
    bundleEntryBuilder.setRequestCondition('ifMatch', '2013-04-15T15:24:23')
    bundleEntryBuilder.setMethod('GET')
    const entry = bundleEntryBuilder.build()

    expect(entry.request).to.deep.equal({
      ifMatch: '2013-04-15T15:24:23',
      method: 'GET',
    })
  })
  it('can not set request condition for ifMatch with invalid value', () => {
    expect(() =>
      bundleEntryBuilder.setRequestCondition('ifMatch', 'INVALID')
    ).to.throw()
  })
  it('can not set request condition for invalid condition', () => {
    expect(() =>
      bundleEntryBuilder.setRequestCondition('INVALID', 'value')
    ).to.throw()
  })
  it('can remove request condition of set condition, if ifMatch is set', () => {
    bundleEntryBuilder.setRequestCondition('ifNoneMatch', 'value')
    bundleEntryBuilder.setRequestCondition('ifMatch', '2013-04-15T15:24:23')
    bundleEntryBuilder.setMethod('GET')
    let entry = bundleEntryBuilder.build()

    expect(entry.request).to.deep.equal({
      ifNoneMatch: 'value',
      ifMatch: '2013-04-15T15:24:23',
      method: 'GET',
    })

    expect(bundleEntryBuilder.removeRequestCondition('ifNoneMatch')).to.equal(
      true
    )
    entry = bundleEntryBuilder.build()

    expect(entry.request).to.deep.equal({
      ifMatch: '2013-04-15T15:24:23',
      method: 'GET',
    })
  })
})
