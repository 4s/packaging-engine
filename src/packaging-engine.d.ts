import { Bundle, code, instant, unsignedInt, BundleEntry } from 'fhir';
import { IdentifierBuilder } from './identifier-builder';
import { PatientBuilder } from './patient-builder';
import { HumanNameBuilder } from './human-name.builder';
import { AddressBuilder } from './address-builder';
import { BundleEntryBuilder } from './bundle-entry-builder';
/** Class representing a PackagingEngine */
export default class PackagingEngine {
    /**
     * The Bundle that is being built
     * @private
     * @type { Bundle }
     * @ignore
     */
    private bundle;
    /**
     * The entry in the bundle pertaining to a patient
     * @private
     * @type { BundleEntry | undefined }
     * @ignore
     */
    private patientEntry;
    /**
     * All entries in the bundle pertaining to observations
     * @private
     * @type { BundleEntry[] }
     * @ignore
     */
    private observationEntries;
    /**
     * All entries in the bundle pertaining to devices
     * @private
     * @type { BundleEntry[] }
     * @ignore
     */
    private deviceEntries;
    /**
     * The entry in the bundle pertaining to a questionnaire response
     * @private
     * @type { BundleEntry | undefined }
     * @ignore
     */
    private questionnaireResponseEntry;
    private fullUrls;
    /**
     * Creates a new PackagingEngine
     */
    constructor();
    /**
     * Returns an IdentifierBuilder for creating Identifier resources
     * @returns { IdentifierBuilder } The IdentifierBuilder
     */
    getIdentifierBuilder(): IdentifierBuilder;
    /**
     * Returns a PatientBuilder for creating Patient resources
     * @returns { PatientBuilder } The PatientBuilder
     */
    getPatientBuilder(): PatientBuilder;
    /**
     * Returns a HumanNameBuilder for creating HumanName resources
     * @returns { HumanNameBuilder } The HumanNameBuilder
     */
    getHumanNameBuilder(): HumanNameBuilder;
    /**
      * Returns an AddressBuilder for creating Address resources
      * @returns { AddressBuilder } The AddressBuilder
      */
    getAddressBuilder(): AddressBuilder;
    /**
     * Returns a BundleEntryBuilder for creating entries for the bundle
     * @returns { BundleEntryBuilder } The BundleEntryBuilder
     */
    getEntryBuilder(): BundleEntryBuilder;
    /**
     * Sets the type of package to create. The type must be part of the following set:
     * document | message | transaction | transaction-response | batch | batch-response | history | searchset | collection
     * @param { code } type - The type to set
     * @returns { PackagingEngine } - this PackagingEngine
     */
    setPackageType(type: code): PackagingEngine;
    /**
     * Sets the time this package was assembled
     * The timestamp must be formatted as follows: YYYY-MM-DDThh:mm:ss.sss+zz:zz
     * @param { timestamp } timestamp - the time this package was assembled
     * @returns { PackagingEngine } - this PackagingEngine
     */
    setTimestamp(timestamp: instant): PackagingEngine;
    /**
     * Sets the total number of matches (used when the type is searchset)
     * @param { unsignedInt } total - The total number of matches
     * @returns { PackagingEngine } - this PackagingEngine
     */
    setTotal(total: unsignedInt): PackagingEngine;
    /**
     * Add an entry to the package. The following resources are currently accepted:
     * Patient | Observation | Device | QuestionnaireResponse
     * @param { code } method - The HTTP-method to be used in this package. The method must be one of the following:
     * GET | POST | PUT | DELETE
     * @param { uri } fullUrl - The Absolute URL for the resource.
     * The fullUrl SHALL NOT disagree with the id in the resource -
     * i.e. if the fullUrl is not a urn:uuid, the URL shall be version-independent URL consistent with the Resource.id.
     * The fullUrl is a version independent reference to the resource
     * @param { Patient | Observation | Device | QuestionnaireResponse } resource - The resource the entry revolves around
     * @param { uri } url - The URL for this entry, relative to the root (the address to which the request is posted)
     * @param { string } ifNoneMatch - If the ETag values match, return a 304 Not Modified status
     * @param { string } ifModifiedSince - Only perform the operation if the last updated date matches
     * @param { string } ifMatch - Only perform the operation if the Etag value matches
     * @param { string } ifNoneExist -Instruct the server not to perform the create if a specified resource already exists.
     * (This is just the query portion of the URL - what follows the "?" (not including the "?").)
     */
    addRequestEntry(entry: BundleEntry): PackagingEngine;
    /**
     * Packages the resources
     * @returns { Bundle } - the packaged resources
     */
    package(): Bundle;
    /**
     * Creates a bundle
     * @ignore
     */
    private make;
    private applyPatientReference;
    /**
     * Sets references from all entries to the patiententry, if one exists
     * @ignore
     */
    private setReferences;
}
