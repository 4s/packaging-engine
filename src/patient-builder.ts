import {
  Patient,
  instant,
  Identifier,
  uri,
  HumanName,
  code,
  ContactPoint,
  positiveInt,
  dateTime,
  date,
  Address,
  CodeableConcept,
  integer,
  HumanNameUse, 
  ContactPointUse, 
  ContactPointSystem,
  PatientGender,
  AddressUse,
  AddressType
} from 'fhir'
import { Guid } from './helpers/guid'

/** Class representing a FHIR Address builder */
export class PatientBuilder {
  /**
   * The Patient that is being built
   * @private
   * @type { Patient }
   * @ignore
   */
  private patient: Patient

  /**
   * Creates a new FHIR PatientBuilder
   */
  public constructor() {
    this.patient = this.make()
  }

  /**
   * Returns all identifiers for patient
   * @returns { Identifier[] } - the identifiers
   */
  public getIdentifiers(): Identifier[] {
    if (Array.isArray(this.patient.identifier)) {
      return this.patient.identifier
    }
    return []
  }

  /**
   * Returns identifier from specific system if one exists
   * @param { uri } system - the system from which to return the identifier
   * @returns { Identifier } the identifier
   */
  public getIdentifier(system: uri): Identifier | undefined {
    let returnIdentifier: Identifier | undefined
    if (!this.patient || !this.patient.identifier) return
    this.patient.identifier.forEach(identifier => {
      if (identifier.system === system) {
        returnIdentifier = identifier
        return returnIdentifier
      }
    })
    return returnIdentifier
  }

  /**
   * Replaces the identifier array with a new array containing a single identifier
   * @param { Identifier } identifier - the identifier to be set
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setIdentifier(identifier: Identifier): PatientBuilder {
    this.patient.identifier = [identifier]
    return this
  }

  /**
   * Appends an identifier to the patient, if an identifier within the system does not already exist
   * @param { Identifier } identifier - the identifier to be appended
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public addIdentifier(identifier: Identifier): PatientBuilder {
    if (!identifier.system || !identifier.value) throw new Error('Identifier must have both system and value')
    if (Array.isArray(this.patient.identifier)) {
      if (!this.getIdentifier(identifier.system)) {
        this.patient.identifier.push(identifier)
      } else {
        throw new Error(
          'An identifier in that system already exists on the patient.'
        )
      }
    } else {
      this.patient.identifier = [identifier]
    }
    return this
  }

  /**
   * Removes an identifier specified by the system-property, if one exists
   * @param { uri } system - the system property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeIdentifier(system: uri): boolean {
    let returnValue = false
    if (!this.patient || !this.patient.identifier) return returnValue
    this.patient.identifier.forEach((identifier, index) => {
      if (identifier.system === system) {
        if (!this.patient || !this.patient.identifier) return returnValue
        this.patient.identifier.splice(index, 1)
        returnValue = true
      }
    })
    return returnValue
  }

  /**
   * Sets whether or not the patient is active
   * @param { boolean } active - a boolean indicating whether or not the patient is active
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setActive(active: boolean): PatientBuilder {
    this.patient.active = active
    return this
  }

  /**
   * Removes the active field on the patient
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeActive(): boolean {
    delete this.patient.active
    return true
  }

  /**
   * Returns all names for the patient
   * @returns { HumanName[] } - the names
   */
  public getNames(): HumanName[] {
    if (Array.isArray(this.patient.name)) {
      return this.patient.name
    }
    return []
  }

  /**
   * Returns name from specific use, if one exists
   * @param { code } use - the use from which to return the name
   * @returns { HumanName } the name
   */
  public getName(use: HumanNameUse): HumanName | undefined {
    if (!this.patient.name) return
    let returnName: HumanName | undefined
    this.patient.name.forEach(name => {
      if (name.use === use) {
        returnName = name
        return
      }
    })
    return returnName
  }

  /**
   * Replaces the name array with a new array containing a single name
   * @param { HumanName } name - the name to be set
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setName(name: HumanName): PatientBuilder {
    if (
      name.use !== 'usual' &&
      name.use !== 'official' &&
      name.use !== 'temp' &&
      name.use !== 'nickname' &&
      name.use !== 'anonymous' &&
      name.use !== 'old' &&
      name.use !== 'maiden'
    ) {
      throw new Error('The provided use is not part of the valid set of uses')
    }
    this.patient.name = [name]
    return this
  }

  /**
   * Appends a name to the patient, if an name with that use does not already exist
   * @param { HumanName } name - the name to be appended
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public addName(name: HumanName): PatientBuilder {
    if (
      name.use !== 'usual' &&
      name.use !== 'official' &&
      name.use !== 'temp' &&
      name.use !== 'nickname' &&
      name.use !== 'anonymous' &&
      name.use !== 'old' &&
      name.use !== 'maiden'
    ) {
      throw new Error('The provided use is not part of the valid set of uses')
    }
    if (Array.isArray(this.patient.name)) {
      if (!this.getName(name.use)) {
        this.patient.name.push(name)
      } else {
        throw new Error('A name with that use already exists on the patient.')
      }
    } else {
      this.patient.name = [name]
    }
    return this
  }

  /**
   * Removes a name specified by the use-property, if one exists
   * @param { code } use - the use property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeName(use: ContactPointUse): boolean {
    if (!this.patient.name) return false
    this.patient.name.filter(name => name.use !== use)
    return true
  }

  /**
   * Returns all contact details for the patient
   * @returns { ContactPoint[] } - the telecoms
   */
  public getTelecoms(): ContactPoint[] {
    if (Array.isArray(this.patient.telecom)) {
      return this.patient.telecom
    }
    return []
  }

  /**
   * Returns all contact details for the patient for a specific use. The valid uses are as follows:
   * home | work | temp | old | mobile
   * @param { code } use - the use from which to return the telecoms
   * @returns { ContactPoint } the telecom
   */
  public getTelecomsFromUse(use: ContactPointUse): ContactPoint[] {
    if (!this.patient.telecom) return []
    const telecoms: ContactPoint[] = this.patient.telecom.filter(telecom => telecom.use === use)
    return telecoms
  }

  /**
   * Replaces the telecom array with a new array containing a single telecom
   * @param { code } system - phone | fax | email | pager | url | sms | other (the type of telecom)
   * @param { string } value - The actual contact point details
   * @param { code } use - home | work | temp | old | mobile (purpose of this contact point)
   * @param { positiveInt } rank - Specifies preferred order of use (1 = highest)
   * @param { dateTime } start - Start of time period when the contact point was/is in use
   * @param { dateTime } end - End of time period when the contact point was/is in use
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setTelecom(
    system?: code,
    value?: string,
    use?: code,
    rank?: positiveInt,
    start?: dateTime,
    end?: dateTime
  ): PatientBuilder {
    if (
      system !== 'phone' &&
      system !== 'fax' &&
      system !== 'email' &&
      system !== 'pager' &&
      system !== 'url' &&
      system !== 'sms' &&
      system !== 'other'
    ) {
      throw new Error(
        'The provided system is not part of the valid set of systems'
      )
    }
    if (
      use !== 'home' &&
      use !== 'work' &&
      use !== 'temp' &&
      use !== 'old' &&
      use !== 'mobile'
    ) {
      throw new Error('The provided use is not part of the valid set of uses')
    }

    this.patient.telecom = [
      {
        system,
        value,
        use,
        rank,
        period: {
          start,
          end,
        },
      } as ContactPoint,
    ]
    return this
  }

  /**
   * Appends a contact detail to the telecom array of the patient, if a telecom with the same system and use
   * is not already defined
   * @param { code } system - phone | fax | email | pager | url | sms | other (the type of contact detail)
   * @param { string } value - The actual contact detail
   * @param { code } use - home | work | temp | old | mobile (purpose of this contact detail)
   * @param { positiveInt } rank - Specifies preferred order of use (1 = highest)
   * @param { dateTime } start - Start of time period when the contact detail was/is in use
   * @param { dateTime } end - End of time period when the contact detail was/is in use
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public addTelecom(
    system?: ContactPointSystem,
    value?: string,
    use?: ContactPointUse,
    rank?: positiveInt,
    start?: dateTime,
    end?: dateTime
  ): PatientBuilder {
    if (!Array.isArray(this.patient.telecom)) {
      this.patient.telecom = []
    }

    this.patient.telecom.forEach(telecom => {
      if (telecom.use === use && telecom.system === system) {
        throw new Error('A telecom already exists with that system and use.')
      }
    })

    this.patient.telecom.push({
      system,
      value,
      use,
      rank,
      period: {
        start,
        end,
      },
    } as ContactPoint)
    return this
  }

  /**
   * Removes a telecom specified by the use- and the system-property, if one exists
   * @param { code } use - home | work | temp | old | mobile (purpose of this contact detail)
   * @param { code } system - phone | fax | email | pager | url | sms | other (the type of contact detail)
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeTelecom(system: ContactPointSystem, use: ContactPointUse): boolean {
    if (!this.patient.telecom) return false
    this.patient.telecom.filter(telecom => ((telecom.use !== use && telecom.system !== system)))
    return false
  }

  /**
   * Sets the gender of the patient 
   * @param gender the gender to be set
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setGender(gender: PatientGender): PatientBuilder {
    this.patient.gender = gender
    return this
  }

  /**
   * Removes the gender of the patient
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeGender(): boolean {
    delete this.patient.gender
    return true
  }

  /**
   * Sets the date of birth of the patient
   * @param { date } birthDate - the date of birth to be set
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setBirthdate(birthDate: date): PatientBuilder {
    this.patient.birthDate = birthDate
    return this
  }

  /**
   * Removes the birthdate from the patient
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeBirthdate(): boolean {
    delete this.patient.birthDate
    return true
  }

  /**
   * Sets a boolean indicating whether the patient is deceased
   * @param { boolean } deceased - the boolean
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setDeceasedBoolean(deceased: boolean): PatientBuilder {
    this.patient.deceasedBoolean = deceased
    return this
  }

  /**
   * Removes the boolean indicating whether the patient is deceased
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDeceasedBoolean(): boolean {
    delete this.patient.deceasedBoolean
    return true
  }

  /**
   * Sets a dateTime indicating the time and date of death of the patient
   * @param { dateTime } deceased - the dateTime
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setDeceasedDateTime(deceased: dateTime): PatientBuilder {
    this.patient.deceasedDateTime = deceased
    return this
  }

  /**
   * Removes the dateTime indicating the time and date of death of the patient
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDeceasedDateTime(): boolean {
    delete this.patient.deceasedDateTime
    return true
  }

  /**
   * Returns all addresses for the patient
   * @returns { Address[] } - the addresses
   */
  public getAddresses(): Address[] {
    if (Array.isArray(this.patient.address)) {
      return this.patient.address
    }
    return []
  }

  /**
   * Returns name from specific use and of specific type, if one exists.
   * Use must be one of the following: home | work | temp | old.
   * Type must be one of the following: postal | physical | both.
   * @param { code } use - the use from which to return the address
   * @param { code } type - the type of address to return
   * @returns { Address } the address
   */
  public getAddress(use: AddressUse, type: AddressType): Address | undefined {
    let address: Address = {}
    if (!this.patient.address) return
    this.patient.address.forEach(add => {
      if (add.use === use && add.type === type) {
        address = add
      }
    })
    return address
  }

  /**
   * Replaces the address array with a new array containing a single address
   * @param { Address } address - the address to be set
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setAddress(address: Address): PatientBuilder {
    this.patient.address = [address]
    return this
  }

  /**
   * Appends an address to the patient, if an address with that use and type does not already exist
   * @param { Address } address - the address to be appended
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public addAddress(address: Address): PatientBuilder {
    if (this.patient.address) {
      if (address.use && address.type) {
        if (!this.getAddress(address.use, address.type)) {
          this.patient.address.push(address)
        } else {
          throw new Error('A name with that use already exists on the patient.')
        }  
      } else {
        this.patient.address = [address]
      }
    } else {
      this.patient.address = [address]
    }
    return this
  }

  /**
   * Removes a name specified by the use-property and a type-property, if one exists
   * @param { code } use - the use property
   * @param { code } type - the type property
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeAddress(use: AddressUse, type: AddressType): boolean {
    if (!this.patient.address) return false
    this.patient.address.filter(address => (address.use !== use || address.type !== type))
    return true
  }

  /**
   * Sets the marital status for the patient
   * @param { string } text - Plain text representation of the type of observation
   * @param { uri } system - Identity of the terminology system
   * @param { string } version - Contains extended information for property 'system'
   * @param { code } code - Symbol in syntax defined by the system
   * @param { string } display - Representation defined by the system
   * @param { boolean } userSelected - If this coding was chosen directly by the user
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setMaritalStatus(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): PatientBuilder {
    const codeableConcept: CodeableConcept = {
      coding: [
        {
          system,
          version,
          code,
          display,
          userSelected,
        },
      ],
      text,
    } as CodeableConcept
    this.patient.maritalStatus = codeableConcept
    return this
  }

  /**
   * Removes the marital status from the patient
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeMaritalStatus(): boolean {
    delete this.patient.maritalStatus
    return true
  }

  /**
   * Sets a boolean indicating whether the patient is part of a multiple birth
   * @param { boolean } multipleBirth - the boolean
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setMultipleBirthBoolean(multipleBirth: boolean): PatientBuilder {
    this.patient.multipleBirthBoolean = multipleBirth
    return this
  }

  /**
   * Removes the boolean indicating whether the patient is part of a multiple birth
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeMultipleBirthBoolean(): boolean {
    delete this.patient.multipleBirthBoolean
    return true
  }

  /**
   * Sets an integer to indicate multiple birth order
   * @param { integer } multipleBirth - the integer
   * @returns { PatientBuilder } - this PatientBuilder
   */
  public setMultipleBirthInteger(multipleBirth: integer): PatientBuilder {
    this.patient.multipleBirthInteger = multipleBirth
    return this
  }

  /**
   * Removes the integer to indicate multiple birth order
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeMultipleBirthInteger(): boolean {
    delete this.patient.multipleBirthInteger
    return true
  }

  /**
   * Builds and returns the patient
   * @returns { Patient } - the patient
   */
  public build(): Patient {
    return this.patient
  }

  /**
   * Creates a patient
   * @ignore
   */
  private make(): Patient {
    const now: instant = new Date().toISOString().toString()
    const patient = {
      resourceType: 'Patient',
      meta: {
        lastUpdated: now,
      }
    } as Patient
    return patient
  }
}
