# Healthcare Resource Packaging Engine
This library functions as an easy way of packaging healthcare resources.

This README.md will explain the basics of installing, building and testing this library.
Beyond that, the repository also contains a more detailed functionality and design description, as well as TypeDoc
documentation in the form of an HTML page in the docs folder located in this library or at:
https://bitbucket.org/4s/packaging-engine/src/master/docs/

## Installation
This library can be installed in two ways; using npm or manually.

### Using npm
You need npm installed on your machine (https://www.npmjs.com/get-npm)

In your project directory type the command to install version 1.2.8
```bash
npm install bitbucket:4s/packaging-engine#v1.2.8
```

### Manual installation
The sourcecode of this repository is written in typescript and will therefore have to be transpiled to Javascript
in order to be used in a project.

a transpiled version is available in the dist folder. Therefore, to use the already built version of this library,
just download dist/packaging-engine.js and include it in your HTML file:
```html
<script src="path/to/packaging-engine/dist/packaging-engine.js"></script>
```
a sourcemap is also included in the dist-folder for debuggability.

## Making changes and building
If changes are made to the sourcecode, the bundled files have to be rebuilt. Webpack (https://github.com/webpack/webpack)
and typescript (https://www.typescriptlang.org/) are used for bundling and transpiling,
and in order to use these frameworks, npm is required (https://www.npmjs.com/get-npm).

Follow the steps below in your terminal/command prompt to build the project:

1\. Download or clone the project directory onto your machine and cd into it:
```bash
git clone https://bitbucket.org/4s/packaging-engine.git && cd packaging-engine
```
2\. Install the abovementioned dependencies:
```bash
npm install
```
(2\.a)
```
**Make changes to the project**
```
3\. Build the project
```bash
npm run build
```
The output of the build process will be located in the dist-directory

## Versioning
To release a new version use the npm version command, for example to create a new patch version, run:
```bash
npm version patch
```
Then push with --follow-tags parameter

## Testing
Testing is performed using the Karma testrunner (https://karma-runner.github.io/2.0/index.html),
the Jasmine framework (https://jasmine.github.io/) and Webpack (https://github.com/webpack/webpack)
and babel (https://www.typescriptlang.org/) for bundling and transpiling.

Therefore, to perform tests, you need to have npm installed on your machine (https://www.npmjs.com/get-npm)

To perform the tests follow the steps below in your terminal/command prompt:

1\. Download or clone the project directory onto your machine and cd into it:
```bash
git clone https://bitbucket.org/4s/ packaging-engine.git && cd  packaging-engine
```
2\. Install the abovementioned dependencies:
```bash
npm install
```
3\. Run the tests:
```bash
npm run test
```
The output from the test should appear directly in the terminal.

## Support & Ownership
Any questions regarding this library should be directed at [Simon H. Madsen]
simon.madsen@alexandra.dk