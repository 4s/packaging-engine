import {
  BundleEntry,
  uri,
  code,
  BundleEntryRequest,
  instant,
  Patient,
  Observation,
  Device,
  QuestionnaireResponse,
  Identifier,
} from 'fhir'

/** Class representing a FHIR BundleEntryBuilder */
export class BundleEntryBuilder {
  /**
   * The bundleEntry being built
   * @private
   * @type { BundleEntry }
   * @ignore
   */
  private bundleEntry: BundleEntry

  /**
   * Creates a new FHIR BundleEntryBuilder
   */
  public constructor() {
    this.bundleEntry = this.make()
  }

  /**
   * Sets the The Absolute URL for the resource of the bundle entry. The fullUrl element SHALL have a value except that:
   * fullUrl can be empty on a POST
   * Results from operations might involve resources that are not identified.
   * @param { uri } fullUrl - the start dateTime of the period
   * @returns { BundleEntryBuilder } - this BundleEntryBuilder
   */
  public setFullUrl(fullUrl: uri): BundleEntryBuilder {
    this.bundleEntry.fullUrl = fullUrl
    return this
  }

  /**
   * Removes the The Absolute URL for the resource of the bundle entry.
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeFullUrl(): boolean {
    delete this.bundleEntry.fullUrl
    return true
  }

  /**
   * Sets the HTTP action to be executed for this entry.
   * @param { code } method - The HTTP action to be executed.
   * @returns { BundleEntryBuilder } - this BundleEntryBuilder
   */
  public setMethod(method: code): BundleEntryBuilder {
    if (!this.bundleEntry.request) {
      this.bundleEntry.request = {} as BundleEntryRequest
    }
    switch (method) {
      case 'GET':
      case 'POST':
        this.bundleEntry.request.method = method
        if (
          this.bundleEntry.resource &&
          this.bundleEntry.resource.resourceType
        ) {
          this.bundleEntry.request.url = this.bundleEntry.resource.resourceType
        }
        break
      case 'PUT':
      case 'DELETE':
        if (!this.bundleEntry.resource || !this.bundleEntry.resource.id) {
          throw new Error(
            `When using ${method}-requests, resource must have an id.`
          )
        }
        this.bundleEntry.request.method = method
        this.bundleEntry.request.url = `${
          this.bundleEntry.resource.resourceType
        }/${this.bundleEntry.resource.id}`
        break
      default:
        throw new Error(
          'The provided method is not part of the valid set of methods.'
        )
    }
    return this
  }

  /**
   * Sets the condition for the request.
   * @param { string } condition - The type of condition. The condition must have one of the following values:
   * ifNoneMatch | ifModifiedSince | ifMatch | ifNoneExist
   * @param { string | instant } value - The value for the condition. This should be either an identifier if the condition is
   * ifNoneMatch, ifMatch or ifNoneExit, or an instant if the condition is ifModifiedSince
   * @returns { BundleEntryBuilder } this BundleEntryBuilder
   */
  public setRequestCondition(
    condition: string,
    value: string | instant
  ): BundleEntryBuilder {
    if (!this.bundleEntry.request) {
      this.bundleEntry.request = {} as BundleEntryRequest
    }
    switch (condition) {
      case 'ifNoneMatch':
        this.bundleEntry.request.ifNoneMatch = value
        break
      case 'ifModifiedSince':
        this.bundleEntry.request.ifModifiedSince = value
        break
      case 'ifMatch':
        const regex: RegExp = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$/g
        if (!regex.test(value)) {
          throw new Error(
            'Value not formatted correctly. It should be in the format YYYY-MM-DDThh:mm:ss.sss+zz:zz'
          )
        }
        this.bundleEntry.request.ifMatch = value
        break
      case 'ifNoneExist':
        this.bundleEntry.request.ifNoneExist = value
        break
      default:
        delete this.bundleEntry.request
        throw new Error(
          'The provided condition is not part of the valid set of conditions'
        )
    }
    return this
  }

  /**
   * Removes a request condition based on the type of condition, given as a string argument to this method
   * @param { string } condition - The type of condition. The condition must have one of the following values:
   * ifNoneMatch | ifModifiedSince | ifMatch | ifNoneExist
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeRequestCondition(condition: string): boolean {
    let returnValue = false
    if (this.bundleEntry.request !== undefined) {
      switch (condition) {
        case 'ifNoneMatch':
          delete this.bundleEntry.request.ifNoneMatch
          returnValue = true
          break
        case 'ifModifiedSince':
          delete this.bundleEntry.request.ifModifiedSince
          returnValue = true
          break
        case 'ifMatch':
          delete this.bundleEntry.request.ifMatch
          returnValue = true
          break
        case 'ifNoneExists':
          delete this.bundleEntry.request.ifNoneExist
          returnValue = true
          break
      }
    }
    return returnValue
  }

  /**
   * Sets the resource of the entry
   * @param { Patient | Observation | Device | QuestionnaireResponse } resource - The resource to be set
   * @returns { BundleEntryBuilder } this BundleEntryBuilder
   */
  public setResource(
    resource: Patient | Observation | Device | QuestionnaireResponse
  ): BundleEntryBuilder {
    if (
      this.bundleEntry.request &&
      (this.bundleEntry.request.method === 'GET' ||
        this.bundleEntry.request.method === 'PUT') &&
      resource.id === undefined
    ) {
      throw new Error(
        `When using ${
          this.bundleEntry.request.method
        }-requests, resource must have an id.`
      )
    }

    switch (resource.resourceType) {
      case 'Patient':
      case 'Observation':
      case 'Device':
      case 'QuestionnaireResponse':
        this.bundleEntry.resource = resource
        break
      default:
        throw new Error(
          'The provided resource is not part of the valid set of resources or does not have the resourceType specified.'
        )
    }
    return this
  }

  /**
   * Builds and returns the bundleEntry
   * @returns { BundleEntry } - the bundleEntry
   */
  public build(): BundleEntry {
    return this.bundleEntry
  }

  /**
   * Creates an BundleEntry
   * @ignore
   */
  private make(): BundleEntry {
    const bundleEntry: BundleEntry = {} as BundleEntry
    return bundleEntry
  }
}
