import { expect } from 'chai'
import { IdentifierBuilder } from '../src/identifier-builder'

describe('identifier-builder', () =>{
    let identifierBuilder: IdentifierBuilder

    beforeEach(() => {
        identifierBuilder = new IdentifierBuilder()
    })

    it('can set valid use', () => {
        identifierBuilder.setUse('usual')
        expect(identifierBuilder.build().use).to.equal('usual')
        identifierBuilder.setUse('official')
        expect(identifierBuilder.build().use).to.equal('official')
        identifierBuilder.setUse('temp')
        expect(identifierBuilder.build().use).to.equal('temp')
        identifierBuilder.setUse('secondary')
        expect(identifierBuilder.build().use).to.equal('secondary')
    })
    it('can not set invalid use', () => {
        expect(() => identifierBuilder.setUse('INVALID')).to.throw()
    })

})