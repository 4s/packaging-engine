const path = require('path');
module.exports = {
    entry: './src/packaging-engine.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    output: {
      library: 'packaging-engine',
      libraryTarget: 'umd',
      filename: 'packaging-engine.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map"
};