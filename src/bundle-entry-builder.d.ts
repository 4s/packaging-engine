import { BundleEntry, uri, code, instant, Patient, Observation, Device, QuestionnaireResponse } from 'fhir';
/** Class representing a FHIR BundleEntryBuilder */
export declare class BundleEntryBuilder {
    /**
     * The bundleEntry being built
     * @private
     * @type { BundleEntry }
     * @ignore
     */
    private bundleEntry;
    /**
     * Creates a new FHIR BundleEntryBuilder
     */
    constructor();
    /**
     * Sets the The Absolute URL for the resource of the bundle entry. The fullUrl element SHALL have a value except that:
     * fullUrl can be empty on a POST
     * Results from operations might involve resources that are not identified.
     * @param { uri } fullUrl - the start dateTime of the period
     * @returns { BundleEntryBuilder } - this BundleEntryBuilder
     */
    setFullUrl(fullUrl: uri): BundleEntryBuilder;
    /**
     * Removes the The Absolute URL for the resource of the bundle entry.
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeFullUrl(): boolean;
    /**
     * Sets the HTTP action to be executed for this entry.
     * @param { code } method - The HTTP action to be executed.
     * @returns { BundleEntryBuilder } - this BundleEntryBuilder
     */
    setMethod(method: code): BundleEntryBuilder;
    /**
     * Sets the condition for the request.
     * @param { string } condition - The type of condition. The condition must have one of the following values:
     * ifNoneMatch | ifModifiedSince | ifMatch | ifNoneExist
     * @param { string | instant } value - The value for the condition. This should be either an identifier if the condition is
     * ifNoneMatch, ifMatch or ifNoneExit, or an instant if the condition is ifModifiedSince
     * @returns { BundleEntryBuilder } this BundleEntryBuilder
     */
    setRequestCondition(condition: string, value: string | instant): BundleEntryBuilder;
    /**
     * Removes a request condition based on the type of condition, given as a string argument to this method
     * @param { string } condition - The type of condition. The condition must have one of the following values:
     * ifNoneMatch | ifModifiedSince | ifMatch | ifNoneExist
     * @returns { boolean } a boolean indicating whether the operation was successful
     */
    removeRequestCondition(condition: string): boolean;
    /**
     * Sets the resource of the entry
     * @param { Patient | Observation | Device | QuestionnaireResponse } resource - The resource to be set
     * @returns { BundleEntryBuilder } this BundleEntryBuilder
     */
    setResource(resource: Patient | Observation | Device | QuestionnaireResponse): BundleEntryBuilder;
    /**
     * Builds and returns the bundleEntry
     * @returns { BundleEntry } - the bundleEntry
     */
    build(): BundleEntry;
    /**
     * Creates an BundleEntry
     * @ignore
     */
    private make;
}
