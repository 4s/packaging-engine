import { Patient, Identifier, uri, HumanName, code, ContactPoint, positiveInt, dateTime, date, Address, integer } from 'fhir';
/** Class representing a FHIR Address builder */
export declare class PatientBuilder {
    /**
     * The Patient that is being built
     * @private
     * @type { Patient }
     * @ignore
     */
    private patient;
    /**
     * Creates a new FHIR PatientBuilder
     */
    constructor();
    /**
     * Returns all identifiers for patient
     * @returns { Identifier[] } - the identifiers
     */
    getIdentifiers(): Identifier[];
    /**
     * Returns identifier from specific system if one exists
     * @param { uri } system - the system from which to return the identifier
     * @returns { Identifier } the identifier
     */
    getIdentifier(system: uri): Identifier;
    /**
     * Replaces the identifier array with a new array containing a single identifier
     * @param { Identifier } identifier - the identifier to be set
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setIdentifier(identifier: Identifier): PatientBuilder;
    /**
     * Appends an identifier to the patient, if an identifier within the system does not already exist
     * @param { Identifier } identifier - the identifier to be appended
     * @returns { PatientBuilder } - this PatientBuilder
     */
    addIdentifier(identifier: Identifier): PatientBuilder;
    /**
     * Removes an identifier specified by the system-property, if one exists
     * @param { uri } system - the system property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeIdentifier(system: uri): boolean;
    /**
     * Sets whether or not the patient is active
     * @param { boolean } active - a boolean indicating whether or not the patient is active
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setActive(active: boolean): PatientBuilder;
    /**
     * Removes the active field on the patient
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeActive(): boolean;
    /**
     * Returns all names for the patient
     * @returns { HumanName[] } - the names
     */
    getNames(): HumanName[];
    /**
     * Returns name from specific use, if one exists
     * @param { code } use - the use from which to return the name
     * @returns { HumanName } the name
     */
    getName(use: code): HumanName;
    /**
     * Replaces the name array with a new array containing a single name
     * @param { HumanName } name - the name to be set
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setName(name: HumanName): PatientBuilder;
    /**
     * Appends a name to the patient, if an name with that use does not already exist
     * @param { HumanName } name - the name to be appended
     * @returns { PatientBuilder } - this PatientBuilder
     */
    addName(name: HumanName): PatientBuilder;
    /**
     * Removes a name specified by the use-property, if one exists
     * @param { code } use - the use property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeName(use: code): boolean;
    /**
     * Returns all contact details for the patient
     * @returns { ContactPoint[] } - the telecoms
     */
    getTelecoms(): ContactPoint[];
    /**
     * Returns all contact details for the patient for a specific use. The valid uses are as follows:
     * home | work | temp | old | mobile
     * @param { code } use - the use from which to return the telecoms
     * @returns { ContactPoint } the telecom
     */
    getTelecomsFromUse(use: code): ContactPoint[];
    /**
     * Replaces the telecom array with a new array containing a single telecom
     * @param { code } system - phone | fax | email | pager | url | sms | other (the type of telecom)
     * @param { string } value - The actual contact point details
     * @param { code } use - home | work | temp | old | mobile (purpose of this contact point)
     * @param { positiveInt } rank - Specifies preferred order of use (1 = highest)
     * @param { dateTime } start - Start of time period when the contact point was/is in use
     * @param { dateTime } end - End of time period when the contact point was/is in use
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setTelecom(system?: code, value?: string, use?: code, rank?: positiveInt, start?: dateTime, end?: dateTime): PatientBuilder;
    /**
     * Appends a contact detail to the telecom array of the patient, if a telecom with the same system and use
     * is not already defined
     * @param { code } system - phone | fax | email | pager | url | sms | other (the type of contact detail)
     * @param { string } value - The actual contact detail
     * @param { code } use - home | work | temp | old | mobile (purpose of this contact detail)
     * @param { positiveInt } rank - Specifies preferred order of use (1 = highest)
     * @param { dateTime } start - Start of time period when the contact detail was/is in use
     * @param { dateTime } end - End of time period when the contact detail was/is in use
     * @returns { PatientBuilder } - this PatientBuilder
     */
    addTelecom(system?: code, value?: string, use?: code, rank?: positiveInt, start?: dateTime, end?: dateTime): PatientBuilder;
    /**
     * Removes a telecom specified by the use- and the system-property, if one exists
     * @param { code } use - home | work | temp | old | mobile (purpose of this contact detail)
     * @param { code } system - phone | fax | email | pager | url | sms | other (the type of contact detail)
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeTelecom(system: code, use: code): boolean;
    /**
     * Sets the gender of the patient
     * @param gender the gender to be set
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setGender(gender: code): PatientBuilder;
    /**
     * Removes the gender of the patient
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeGender(): boolean;
    /**
     * Sets the date of birth of the patient
     * @param { date } birthDate - the date of birth to be set
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setBirthdate(birthDate: date): PatientBuilder;
    /**
     * Removes the birthdate from the patient
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeBirthdate(): boolean;
    /**
     * Sets a boolean indicating whether the patient is deceased
     * @param { boolean } deceased - the boolean
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setDeceasedBoolean(deceased: boolean): PatientBuilder;
    /**
     * Removes the boolean indicating whether the patient is deceased
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDeceasedBoolean(): boolean;
    /**
     * Sets a dateTime indicating the time and date of death of the patient
     * @param { dateTime } deceased - the dateTime
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setDeceasedDateTime(deceased: dateTime): PatientBuilder;
    /**
     * Removes the dateTime indicating the time and date of death of the patient
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDeceasedDateTime(): boolean;
    /**
     * Returns all addresses for the patient
     * @returns { Address[] } - the addresses
     */
    getAddresses(): Address[];
    /**
     * Returns name from specific use and of specific type, if one exists.
     * Use must be one of the following: home | work | temp | old
     * Type must be one of the following: postal | physical | both
     * @param { code } use - the use from which to return the address
     * @param { code } type - the type of address to return
     * @returns { Address } the address
     */
    getAddress(use: code, type: code): Address;
    /**
     * Replaces the address array with a new array containing a single address
     * @param { HumanName } name - the address to be set
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setAddress(address: Address): PatientBuilder;
    /**
     * Appends an address to the patient, if an address with that use and type does not already exist
     * @param { HumanName } name - the address to be appended
     * @returns { PatientBuilder } - this PatientBuilder
     */
    addAddress(address: Address): PatientBuilder;
    /**
     * Removes a name specified by the use-property and a type-property, if one exists
     * @param { code } use - the use property
     * @param { code } type - the type property
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeAddress(use: code, type: code): boolean;
    /**
     * Sets the marital status for the patient
     * @param { string } [text] - Plain text representation of the type of observation
     * @param { uri } [system] - Identity of the terminology system
     * @param { string } [version] - Contains extended information for property 'system'
     * @param { code } [code] - Symbol in syntax defined by the system
     * @param { string } [display] - Representation defined by the system
     * @param { boolean } [userSelected] - If this coding was chosen directly by the user
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setMaritalStatus(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): PatientBuilder;
    /**
     * Removes the marital status from the patient
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeMaritalStatus(): boolean;
    /**
     * Sets a boolean indicating whether the patient is part of a multiple birth
     * @param { boolean } multipleBirth - the boolean
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setMultipleBirthBoolean(multipleBirth: boolean): PatientBuilder;
    /**
     * Removes the boolean indicating whether the patient is part of a multiple birth
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeMultipleBirthBoolean(): boolean;
    /**
     * Sets an integer to indicate multiple birth order
     * @param { integer } multipleBirth - the integer
     * @returns { PatientBuilder } - this PatientBuilder
     */
    setMultipleBirthInteger(multipleBirth: integer): PatientBuilder;
    /**
     * Removes the integer to indicate multiple birth order
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeMultipleBirthInteger(): boolean;
    /**
     * Builds and returns the patient
     * @returns { Patient } - the patient
     */
    build(): Patient;
    /**
     * Creates a patient
     * @ignore
     */
    private make;
}
