import {
  Identifier,
  code,
  uri,
  CodeableConcept,
  Coding,
  dateTime,
  Period,
  Reference,
} from 'fhir'

/** Class representing a FHIR Identifier builder */
export class IdentifierBuilder {
  /**
   * The identifier being built
   * @private
   * @type { Identifier }
   * @ignore
   */
  private identifier: Identifier

  /**
   * Create a new FHIR ObservationBuilder
   */
  public constructor() {
    this.identifier = {} as Identifier
  }

  /**
   * Sets the use of the identifier
   * usual | official | temp | secondary (If known)
   * @param { code } [use] -  the use to set
   */
  public setUse(use: code): IdentifierBuilder {
    if (
      use !== 'usual' &&
      use !== 'official' &&
      use !== 'temp' &&
      use !== 'secondary'
    ) {
      throw new Error('The provided use is not part of the valid set of uses')
    }
    this.identifier.use = use
    return this
  }

  /**
   * Removes the use from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeUse(): boolean {
    delete this.identifier.use
    return true
  }

  /**
   * Sets the type of the identifier as a CodeableConcept
   * @param { string } [text] - Plain text representation of the concept
   * @param { uri } [system] - Identity of the terminology system
   * @param { string } [version] - Version of the system - if relevant
   * @param { code } [code] - Symbol in syntax defined by the system
   * @param { string } [display] - Representation defined by the system
   * @param { boolean } [userSelected] - If this coding was chosen directly by the user
   */
  public setType(
    text?: string,
    system?: uri,
    version?: string,
    code?: code,
    display?: string,
    userSelected?: boolean
  ): IdentifierBuilder {
    const codeableConcept: CodeableConcept = {} as CodeableConcept
    const coding: Coding = {} as Coding

    coding.system = system
    coding.version = version
    coding.code = code
    coding.display = display
    coding.userSelected = userSelected

    codeableConcept.coding = [coding]
    codeableConcept.text = text

    this.identifier.type = codeableConcept
    return this
  }

  /**
   * Removes the type from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeType(): boolean {
    delete this.identifier.type
    return true
  }

  /**
   * Sets the namespace for the identifier value
   * @param { uri } system - the namespace to set
   */
  public setSystem(system: uri): IdentifierBuilder {
    this.identifier.system = system
    return this
  }

  /**
   * Removes the system from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeSystem(): boolean {
    delete this.identifier.system
    return true
  }

  /**
   * Sets the (unique) value of the identifier
   * @param { string } value -  the value to set
   */
  public setValue(value: string): IdentifierBuilder {
    this.identifier.value = value
    return this
  }

  /**
   * Removes the value from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeValue(): boolean {
    delete this.identifier.value
    return true
  }

  /**
   * Sets the period wherin the period is in effect
   * @param { dateTime } [start] - The start of the period
   * @param { dateTime } [end] - The end of the period
   */
  public setPeriod(start?: dateTime, end?: dateTime): IdentifierBuilder {
    this.identifier.period = {
      start,
      end,
    } as Period

    return this
  }

  /**
   * Removes the period from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removePeriod(): boolean {
    delete this.identifier.period
    return true
  }

  /**
   * Sets the assigner of the identifier
   * @param { string } [reference] - Literal reference, Relative, internal or absolute URL
   * @param { Identifier } [identifier] - Logical reference, when literal reference is not known
   * @param { string } [display] - Text alternative for the resource
   */
  public setAssigner(
    reference?: string,
    identifier?: Identifier,
    display?: string
  ): IdentifierBuilder {
    this.identifier.assigner = {
      reference,
      identifier,
      display,
    } as Reference

    return this
  }

  /**
   * Removes the assigner from the identifier
   * @returns { boolean } a boolean indicating whether the operation was successful
   */
  public removeAssigner(): boolean {
    delete this.identifier.assigner
    return true
  }

  /**
   * Returns the identifier
   * @returns { Identifier } the identifier
   */
  public build(): Identifier {
    return this.identifier
  }
}
