import PackagingEngine from '../src/packaging-engine'
import { expect } from 'chai'
import { BundleEntry, Observation, Reference } from 'fhir'

describe('PackagingEngine', () => {
  let packagingEngine: PackagingEngine

  beforeEach(() => {
    packagingEngine = new PackagingEngine()
  })

  it('should add patientreference to both subject and performer in observation', () => {
    const fullUrl = 'aFullUrl'
    const bundle = packagingEngine.addRequestEntry(
      packagingEngine.getEntryBuilder().setResource(
        packagingEngine.getPatientBuilder().addIdentifier(
          packagingEngine.getIdentifierBuilder().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996").build()
        ).build()
      ).setFullUrl(fullUrl).build()
    ).addRequestEntry(
      packagingEngine.getEntryBuilder().setResource(observation).build()
    ).package()

    const entries = bundle.entry as BundleEntry[]

    const patientreference = entries[0].fullUrl
    
    const observationResource = entries[1].resource as Observation
    
    const observationSubject = observationResource.subject as Reference
    const observationPerformer = observationResource.performer as Reference[]

    expect(observationSubject.reference).to.equal(patientreference)
    expect(observationPerformer[0].reference).to.equal(patientreference)

  })

  it('can not add a resource with a fullUrl that match an already added resource', () => {
    const fullUrl = 'aFullUrl'

    expect(() => {
      packagingEngine.addRequestEntry(
          packagingEngine.getEntryBuilder().setResource(device)
          .setFullUrl(fullUrl).build()
        ).addRequestEntry(
          packagingEngine.getEntryBuilder().setResource(device)
          .setFullUrl(fullUrl).build()
        )
    }).to.throw()
  })

  it('cannot add entry without a resource', () => {
    expect(() => packagingEngine.addRequestEntry({})).to.throw()
  })

  it('cannot add entry without a resourceType in the resource', () => {
    expect(() => packagingEngine.addRequestEntry({ resource: {}})).to.throw()
  })
  it('cannot add entry without an invalid resourceType', () => {
    expect(() => packagingEngine.addRequestEntry({ resource: { resourceType: "INVALID"}})).to.throw()
  })

  
  const device = {
    "resourceType": "Device",
    "identifier": [
      {
        "system" : "urn:ietf:rfc:3986",
        "value": "value"
      }
    ],
  }
  

  const observation = {
    "resourceType": "Observation",
    "meta": {
      "profile": [
        "placeholder/phdNumericObservation",
        "http://hl7.org/fhir/StructureDefinition/bp"
      ]
    },
    "identifier": [
      {
        "value": "2512489996-urn:oid:1.2.208.176.1.2-00-09-1F-FE-FF-80-1A-33-150020-122.0-85.0-96.0-20181009T183536.00"
      }
    ],
    "status": "final",
    "code": {
      "coding": [
        {
          "system": "urn:iso:std:iso:11073:10101",
          "code": "150020",
          "display": "MDC_PRESS_BLD_NONINV"
        },{
          "system": "http://loinc.org",
          "code": "85354-9",
          "display": "Blood pressure panel"
        }
      ]
    },
    "effectiveDateTime": "2018-10-09T20:35:36+02:00",
    "component": [
      {
        "code": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "150021",
              "display": "MDC_PRESS_BLD_NONINV_SYS"
            },
            {
              "system": "http://loinc.org",
              "code": "8480-6",
              "display": "Systolic blood pressure"
            }
          ]
        },
        "valueQuantity": {
          "value": "122.0",
          "system": "http://unitsofmeasure.org",
          "code": "mm[Hg]",
          "unit": "mmHg"
        }
      },{
        "code": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "150022",
              "display": "MDC_PRESS_BLD_NONINV_DIA"
            },
            {
              "system": "http://loinc.org",
              "code": "8462-4",
              "display": "Diastolic blood pressure"
            }
          ]
        },
        "valueQuantity": {
          "value": "85.0",
          "system": "http://unitsofmeasure.org",
          "code": "mm[Hg]",
          "unit": "mmHg"
        }
      },{
        "code": {
          "coding": [
            {
              "system": "urn:iso:std:iso:11073:10101",
              "code": "150023",
              "display": "MDC_PRESS_BLD_NONINV_MEAN"
            },
            {
              "system": "http://loinc.org",
              "code": "8478-0",
              "display": "Mean blood pressure"
            }
          ]
        },
        "valueQuantity": {
          "value": "96.0",
          "system": "http://unitsofmeasure.org",
          "code": "mm[Hg]",
          "unit": "mmHg"
        }
      }
    ],
    "device": {
      "reference": "Device/00091FFEFF801A33.00091F801A33"
    },
    "extension": [
      {
        "url": "FIXME-PLACEHOLDER",
        "valueCoding": {
          "code": "automatic",
          "system": "http://example.com/data-entry-method",
          "display": "Måling overført automatisk"
        }
      }
    ]
  }
})


