import { HumanName, code, dateTime, Period } from 'fhir'

/** Class representing a FHIR HumanName builder */
export class HumanNameBuilder {
  /**
   * The HumanName that is being built
   * @private
   * @type { HumanName }
   * @ignore
   */
  private name: HumanName

  /**
   * Creates a new FHIR HumanHumanNameBuilder
   */
  public constructor() {
    this.name = this.make()
  }

  /**
   * Sets the use of this name. The use must be within the following set of valid values:
   * usual | official | temp | nickname | anonymous | old | maiden
   * @param use - the use of the name.
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setUse(use: code): HumanNameBuilder {
    if (
      use !== 'usual' &&
      use !== 'official' &&
      use !== 'temp' &&
      use !== 'nickname' &&
      use !== 'anonymous' &&
      use !== 'old' &&
      use !== 'maiden'
    ) {
      throw new Error('The provided use is not part of the valid set of uses')
    }
    this.name.use = use
    return this
  }

  /**
   * Removes the use from the name
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeUse(): boolean {
    delete this.name.use
    return true
  }

  /**
   * Sets text representation of the full name
   * @param text the text representation of the full name
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setText(text: string): HumanNameBuilder {
    this.name.text = text
    return this
  }

  /**
   * Removes the text representation of the full name
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeText(): boolean {
    delete this.name.text
    return true
  }

  /**
   * Sets the family name of this name
   * @param { string } family - the family name to be set
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setFamilyName(family: string): HumanNameBuilder {
    this.name.family = family
    return this
  }

  /**
   * Removes family name from this name
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeFamilyName(): boolean {
    delete this.name.family
    return true
  }

  /**
   * Returns all given names for patient
   * @returns { string[] } - the given names
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public getGivenNames(): string[] {
    if (Array.isArray(this.name.given)) {
      return this.name.given
    }
    return []
  }

  /**
   * Replaces the given name array with a new array containing a single given name
   * @param { string } name - the given name to be set
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setGivenName(name: string): HumanNameBuilder {
    this.name.given = [name]
    return this
  }

  /**
   * Appends an given name to the patient
   * @param { string } name - the given name to be appended
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public addGivenName(name: string): HumanNameBuilder {
    if (!Array.isArray(this.name.given)) {
      this.name.given = []
    }
    this.name.given.push(name)
    return this
  }

  /**
   * Removes one of the given names if it exists
   * @param { string } name - the name to remove
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeGivenName(name: string): boolean {
    let returnValue = false
    if (!this.name || !this.name.given) return returnValue
    this.name.given.forEach((givenName, index) => {
      if (givenName === name) {
        if (!this.name || !this.name.given) return returnValue
        this.name.given.splice(index, 1)
        returnValue = true
      }
    })
    return returnValue
  }

  /**
   * Returns all prefixes for patient
   * @returns { string[] } - the prefixes
   */
  public getPrefixes(): string[] {
    if (Array.isArray(this.name.prefix)) {
      return this.name.prefix
    }
    return []
  }

  /**
   * Replaces the prefix array with a new array containing a single prefix
   * @param { string } prefix - the prefix to be set
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setPrefixes(prefix: string): HumanNameBuilder {
    this.name.prefix = [prefix]
    return this
  }

  /**
   * Appends an prefix to the name
   * @param { string } prefix - the prefix to be appended
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public addPrefix(prefix: string): HumanNameBuilder {
    if (!Array.isArray(this.name.prefix)) {
      this.name.prefix = []
    }
    this.name.prefix.push(prefix)
    return this
  }

  /**
   * Removes one of the given names if it exists
   * @param { string } name - the name to remove
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removePrefix(prefix: string): boolean {
    let returnValue = false
    if (!this.name || !this.name.prefix) return returnValue
    this.name.prefix.forEach((existingPrefix, index) => {
      if (existingPrefix === prefix) {
        if (!this.name || !this.name.prefix) return returnValue
        this.name.prefix.splice(index, 1)
        returnValue = true
      }
    })
    return returnValue
  }

  /**
   * Returns all suffixes for patient
   * @returns { string[] } - the suffixes
   */
  public getSuffixes(): string[] {
    if (Array.isArray(this.name.suffix)) {
      return this.name.suffix
    }
    return []
  }

  /**
   * Replaces the suffix array with a new array containing a single suffix
   * @param { string } suffix - the suffix to be set
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setSuffixes(suffix: string): HumanNameBuilder {
    this.name.suffix = [suffix]
    return this
  }

  /**
   * Appends an suffix to the name
   * @param { string } suffix - the suffix to be appended
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public addSuffix(suffix: string): HumanNameBuilder {
    if (!Array.isArray(this.name.suffix)) {
      this.name.suffix = []
    }
    this.name.suffix.push(suffix)
    return this
  }

  /**
   * Removes one of the given names if it exists
   * @param { string } name - the name to remove
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeSuffix(suffix: string): boolean {
    let returnValue = false
    if (!this.name || !this.name.suffix) return returnValue
    this.name.suffix.forEach((existingSuffix, index) => {
      if (existingSuffix === suffix) {
        if (!this.name || !this.name.suffix) return returnValue
        this.name.suffix.splice(index, 1)
        returnValue = true
      }
    })
    return returnValue
  }

  /**
   * Sets the time period when name was/is in use
   * If either start or end is left out, the period is considered open-ended.
   * Both should not be left out simoultaneously.
   * @param { dateTime } [start] - the start dateTime of the period
   * @param { dateTime } [end] - the end dateTime of the period
   * @returns { HumanNameBuilder } - this HumanNameBuilder
   */
  public setPeriod(start: dateTime, end: dateTime): HumanNameBuilder {
    const period: Period = {
      start,
      end,
    } as Period
    this.name.period = period
    return this
  }

  /**
   * Removes the time period when name was/is in use
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removePeriod(): boolean {
    delete this.name.period
    return true
  }

  /**
   * Builds and returns the HumanName
   * @returns { HumanName } - the HumanName
   */
  public build(): HumanName {
    return this.name
  }

  /**
   * Creates a HumanName
   * @ignore
   */
  private make(): HumanName {
    const name: HumanName = {} as HumanName
    return name
  }
}
