import { expect } from 'chai'
import { PatientBuilder } from '../src/patient-builder'
import { IdentifierBuilder } from '../src/identifier-builder'
import { Identifier, HumanName } from 'fhir'
import { HumanNameBuilder } from '../src/human-name.builder'
import { AddressBuilder } from '../src/address-builder'

describe('patient-builder', () => {
    let patientBuilder : PatientBuilder

    beforeEach(() => {
        patientBuilder = new PatientBuilder()
    })

    it('can return identifier with given system', () => {
        const identifierBuilder = new IdentifierBuilder
        patientBuilder.setIdentifier(identifierBuilder.setValue('value').setSystem('theSystem').build())

        const returnIdentifier : Identifier = patientBuilder.getIdentifier("theSystem") as Identifier

        expect(returnIdentifier.value).to.equal('value')
    })

    it('can add valid identifier', () => {
        const identifierBuilder = new IdentifierBuilder
        patientBuilder.addIdentifier(identifierBuilder.setValue('value').setSystem('theSystem').build())

        const returnIdentifier : Identifier = patientBuilder.getIdentifier("theSystem") as Identifier
        expect(returnIdentifier.value).to.equal('value')
    })
    it('can not add invalid identifier', () => {
        let identifierBuilder = new IdentifierBuilder
        expect(() => patientBuilder.addIdentifier(identifierBuilder.setSystem('theSystem').build())).to.throw()
        identifierBuilder = new IdentifierBuilder
        expect(() => patientBuilder.addIdentifier(identifierBuilder.setValue('value').build())).to.throw()
    })
    it('can not add identifier when an identifier with the same system is already present', () => {
        const identifierBuilder = new IdentifierBuilder
        patientBuilder.addIdentifier(identifierBuilder.setValue('value').setSystem('theSystem').build())
        expect(() => patientBuilder.addIdentifier(identifierBuilder.setValue('value').setSystem('theSystem').build())).to.throw()
    })
    it('can remove an identifier', () => {
        const identifierBuilder = new IdentifierBuilder
        patientBuilder.addIdentifier(identifierBuilder.setValue('value').setSystem('theSystem').build())

        expect(patientBuilder.removeIdentifier('theSystem')).to.equal(true)
        expect(patientBuilder.getIdentifier("theSystem")).to.equal(undefined)
    })
    it('can set name with valid use', () => {
        let name = new HumanNameBuilder().setUse('usual').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('official').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('temp').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('nickname').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('anonymous').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('old').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        name = new HumanNameBuilder().setUse('maiden').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)
    })
    it('can not add name with use that is already present', () => {
        const name = new HumanNameBuilder().setUse('usual').build()
        expect(patientBuilder.setName(name).getNames()).to.have.length(1)

        expect(() => patientBuilder.addName(name)).to.throw()
    })
    it('can set telecom with valid system and use', () => {
        expect(patientBuilder.setTelecom("phone","value","home").getTelecoms()[0].system).to.equal("phone")
    })
    it('can not set telecom for invalid system', () => {
        expect(() => patientBuilder.setTelecom("INVALID")).to.throw()
    })
    it('can not set telecom for invalid use', () => {
        expect(() => patientBuilder.setTelecom("phone","value","INVALID")).to.throw()
    })
    it('cannot add telecom when one with same system and use exist', () => {
        patientBuilder.addTelecom("phone","value","home")
        expect(() => patientBuilder.addTelecom("phone","value","home")).to.throw()
    })
    it('can add address', () => {
        const addressBuilder = new AddressBuilder()
        patientBuilder.addAddress(addressBuilder.setUse("work").setType("postal").build())
        expect(patientBuilder.getAddress("work","postal")).to.deep.equal({ use: 'work', type: 'postal' })
    })
    it('can not add address if one of same type and use already exist', () => {
        const addressBuilder = new AddressBuilder()
        patientBuilder.addAddress(addressBuilder.setUse("work").setType("postal").build())
        expect(() => patientBuilder.addAddress(addressBuilder.setUse("work").setType("postal").build())).to.throw()
    })
})