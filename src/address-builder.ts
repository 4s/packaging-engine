import { Address, code, dateTime, Period, AddressType } from 'fhir'

/** Class representing a FHIR Address builder */
export class AddressBuilder {
  /**
   * The address being built
   * @private
   * @type { Address }
   * @ignore
   */
  private address: Address

  /**
   * Creates a new FHIR AddressBuilder
   */
  public constructor() {
    this.address = this.make()
  }

  /**
   * Sets the use of this address. The use must be within the following set of valid values:
   * home | work | temp | old
   * @param use - the use of the address.
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setUse(use: code): AddressBuilder {
    if (use !== 'home' && use !== 'work' && use !== 'temp' && use !== 'old') {
      throw new Error('The provided use is not part of the valid set of uses')
    }
    this.address.use = use
    return this
  }

  /**
   * Removes the use from the address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeUse(): boolean {
    delete this.address.use
    return true
  }

  /**
   * Sets the type of the address. The type must be within the following set:
   * postal | physical | both
   * @param { code }type the type of the address
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setType(type: AddressType): AddressBuilder {
    this.address.type = type
    return this
  }

  /**
   * Removes the type of the address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeType(): boolean {
    delete this.address.type
    return true
  }

  /**
   * Sets a text representation of the address
   * @param { string } text - the text to be set
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setText(text: string): AddressBuilder {
    this.address.text = text
    return this
  }

  /**
   * Removes the text representation of the address from this address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeText(): boolean {
    delete this.address.text
    return true
  }

  /**
   * Returns all lines containing street name, number, direction & P.O. Box etc. for this address
   * @returns { string[] } - the lines
   */
  public getLines(): string[] {
    if (Array.isArray(this.address.line)) {
      return this.address.line
    }
    return []
  }

  /**
   * Replaces the lines containing street name, number, direction & P.O. Box etc. for this address
   * with a new array containing a single line containing street name, number, direction & P.O. Box etc.
   * @param { string } line - the line containing street name, number, direction & P.O. Box etc. to be set
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setLines(line: string): AddressBuilder {
    this.address.line = [line]
    return this
  }

  /**
   * Appends a line containing street name, number, direction & P.O. Box etc. to the address
   * @param { string } line - the line containing street name, number, direction & P.O. Box etc. to be appended
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public addLine(line: string): AddressBuilder {
    if (!Array.isArray(this.address.line)) {
      this.address.line = []
    }
    this.address.line.push(name)
    return this
  }

  /**
   * Removes one of the lines containing street name, number, direction & P.O. Box etc. if it exists
   * @param { string } line - line containing street name, number, direction & P.O. Box etc. to be removed
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeLine(line: string): boolean {
    if (!this.address || !this.address.line) return false
    this.address.line.forEach((addressLine, index) => {
      if (addressLine === line) {
        if (!this.address || !this.address.line) return false
        this.address.line.splice(index, 1)
        return true
      }
    })
    return false
  }

  /**
   * Sets the name of the city, town etc.
   * @param { string } city - the the name of the city, town etc. to be set
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setCity(city: string): AddressBuilder {
    this.address.city = city
    return this
  }

  /**
   * Removes the name of the city, town etc. from this address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeCity(): boolean {
    delete this.address.city
    return true
  }

  /**
   * Sets the district name (aka county)
   * @param { string } district - the district name
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setDistrict(district: string): AddressBuilder {
    this.address.district = district
    return this
  }

  /**
   * Removes the district name
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeDistrict(): boolean {
    delete this.address.district
    return true
  }

  /**
   * Sets the sub-unit of the country (abbreviations ok)
   * @param { string } state - the sub-unit of country to be set
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setState(state: string): AddressBuilder {
    this.address.state = state
    return this
  }

  /**
   * Removes the sub-unit of the country
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeState(): boolean {
    delete this.address.state
    return true
  }

  /**
   * Sets the postal code for area
   * @param { string } state - the postal code for area
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setPostalCode(postalCode: string): AddressBuilder {
    this.address.postalCode = postalCode
    return this
  }

  /**
   * Removes the postal code for area for this address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removePostalCode(): boolean {
    delete this.address.postalCode
    return true
  }

  /**
   * Sets the country (e.g. can be ISO 3166 2 or 3 letter code)
   * @param { string } country - the country
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setCountry(country: string): AddressBuilder {
    this.address.country = country
    return this
  }

  /**
   * Removes the country for this address
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removeCountry(): boolean {
    delete this.address.country
    return true
  }

  /**
   * Sets the time period when address was/is in use
   * If either start or end is left out, the period is considered open-ended.
   * Both should not be left out simoultaneously.
   * @param { dateTime } [start] - the start dateTime of the period
   * @param { dateTime } [end] - the end dateTime of the period
   * @returns { AddressBuilder } - this AddressBuilder
   */
  public setPeriod(start: dateTime, end: dateTime): AddressBuilder {
    const period: Period = {
      start,
      end,
    } as Period
    this.address.period = period
    return this
  }

  /**
   * Removes the time period when address was/is in use
   * @returns { boolean } - boolean indicating whether the operation was successful
   */
  public removePeriod(): boolean {
    delete this.address.period
    return true
  }

  /**
   * Builds and returns the address
   * @returns { Address } - the address
   */
  public build(): Address {
    return this.address
  }

  /**
   * Creates an Address
   * @ignore
   */
  private make(): Address {
    const address: Address = {} as Address
    return address
  }
}
