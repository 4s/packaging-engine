# Packaging Engine

This page will go more into details regarding the purpose, current state, use cases etc. of the Packaging Engine

[TOC]

## Purpose

This library was created to make it easier for frontend developers to package healthcare resources together. The library
allows for appending healthcare resources and helps maintain the internal references between the resources. Furthermore,
it contains builders for creating patients easily.

## Documentation

This page along with the README.md function as documentation for the library as a whole. Along with this documentation,
an HTML-page containing documentation for all of the methods available in the library can be found in the docs/html
folder of this library. To use it open the docs/html/index.html page in a browser.

## Maturity level

This library is at a prototype maturity level. All the functionality has been implemented, but there are no guarantees
in relation to the use of this library.

## Current state

At the time of writing, this library has implemented all functionality required in relation to the GMK project.
Some of the fields that are available in the FHIR standard, have not been implemented into the builders yet.

## Usage

The main class of this library is PackagingEngine.

This section will present an example of use for this library

**Creating a patient and add her to the package**

```typescript
import PackagingEngine from 'packaging-engines'

const dme = new PackagingEngine()
const patient = packagingEngine
  .getPatientBuilder()
  .addAddress(
    packagingEngine
      .getAddressBuilder()
      .addLine('Mainstreet 1')
      .setCity('Defaultcity')
      .setDistrict('Regular District')
      .setCountry('Standardland')
      .setPostalCode('11111')
      .setType('both')
      .build()
  )
  .addName(
    packagingEngine
      .getHumanNameBuilder()
      .setGivenName('Nancy')
      .setGivenName('Ann')
      .setFamilyName('Berggren')
      .setUse('official')
      .build()
  )
  .setBirthdate('1948-12-25')
  .setActive(true)
  .setDeceasedBoolean(false)
  .build()

const package = packagingEngine
  .addRequestEntry(
    packagingEngine
      .getEntryBuilder()
      .setMethod('POST')
      .setRequestCondition('ifNoneExists', '_id=' + patient.id)
      .setResource(patient)
      .build()
  )
  .package()
```

For more examples of use see the fhir-engine (https://bitbucket.org/4s/fhir-engine.git)

## License

The PackagingEngine source code is licensed under the Apache 2.0 license
(https://www.apache.org/licenses/LICENSE-2.0).
