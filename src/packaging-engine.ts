import {
  Observation,
  Bundle,
  code,
  instant,
  unsignedInt,
  Device,
  QuestionnaireResponse,
  Reference,
  BundleEntry,
  uri
} from 'fhir'
import { IdentifierBuilder } from './identifier-builder'
import { PatientBuilder } from './patient-builder'
import { HumanNameBuilder } from './human-name.builder'
import { AddressBuilder } from './address-builder'
import { BundleEntryBuilder } from './bundle-entry-builder'
import { Guid } from './helpers/guid'

/** Class representing a PackagingEngine */
export default class PackagingEngine {
  /**
   * The Bundle that is being built
   * @private
   * @type { Bundle }
   * @ignore
   */
  private bundle: Bundle
  /**
   * The entry in the bundle pertaining to a patient
   * @private
   * @type { BundleEntry | undefined }
   * @ignore
   */
  private patientEntry: BundleEntry | undefined
  /**
   * All entries in the bundle pertaining to observations
   * @private
   * @type { BundleEntry[] }
   * @ignore
   */
  private observationEntries: BundleEntry[] 
  /**
   * All entries in the bundle pertaining to devices
   * @private
   * @type { BundleEntry[] }
   * @ignore
   */
  private deviceEntries: BundleEntry[]
  /**
   * The entry in the bundle pertaining to a questionnaire response
   * @private
   * @type { BundleEntry | undefined }
   * @ignore
   */
  private questionnaireResponseEntry: BundleEntry | undefined

  private fullUrls : uri[]

  /**
   * Creates a new PackagingEngine
   */
  public constructor() {
    this.bundle = this.make()
    this.patientEntry = undefined
    this.observationEntries = []
    this.deviceEntries = []
    this.questionnaireResponseEntry = undefined
    this.fullUrls = []
  }

  /**
   * Returns an IdentifierBuilder for creating Identifier resources
   * @returns { IdentifierBuilder } The IdentifierBuilder
   */
  public getIdentifierBuilder(): IdentifierBuilder {
    return new IdentifierBuilder()
  }

  /**
   * Returns a PatientBuilder for creating Patient resources
   * @returns { PatientBuilder } The PatientBuilder
   */
  public getPatientBuilder(): PatientBuilder {
    return new PatientBuilder()
  }

  /**
   * Returns a HumanNameBuilder for creating HumanName resources
   * @returns { HumanNameBuilder } The HumanNameBuilder
   */
  public getHumanNameBuilder(): HumanNameBuilder {
    return new HumanNameBuilder()
  }
  
 /**
   * Returns an AddressBuilder for creating Address resources
   * @returns { AddressBuilder } The AddressBuilder
   */
  public getAddressBuilder(): AddressBuilder {
    return new AddressBuilder()
  }

  /**
   * Returns a BundleEntryBuilder for creating entries for the bundle
   * @returns { BundleEntryBuilder } The BundleEntryBuilder
   */
  public getEntryBuilder(): BundleEntryBuilder {
    return new BundleEntryBuilder()
  }

  /**
   * Sets the type of package to create. The type must be part of the following set:
   * document | message | transaction | transaction-response | batch | batch-response | history | searchset | collection
   * @param { code } type - The type to set
   * @returns { PackagingEngine } - this PackagingEngine
   */
  public setPackageType(type: code): PackagingEngine {
    if (
      type !== 'document' &&
      type !== 'message' &&
      type !== 'transaction' &&
      type !== 'transaction-response' &&
      type !== 'batch' &&
      type !== 'batch-response' &&
      type !== 'history' &&
      type !== 'searchset' &&
      type !== 'collection'
    ) {
      throw new Error('The provided type is not part of the valid set of types')
    }
    this.bundle.type = type
    return this
  }

  /**
   * Sets the time this package was assembled
   * The timestamp must be formatted as follows: YYYY-MM-DDThh:mm:ss.sss+zz:zz
   * @param { timestamp } timestamp - the time this package was assembled
   * @returns { PackagingEngine } - this PackagingEngine
   */
  public setTimestamp(timestamp: instant): PackagingEngine {
    const regex: RegExp = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$/g
    if (!regex.test(timestamp)) {
      throw new Error(
        'Timestamp not formatted correctly. It should be in the format YYYY-MM-DDThh:mm:ss.sss+zz:zz'
      )
    }
    this.bundle.timestamp = timestamp
    return this
  }

  /**
   * Sets the total number of matches (used when the type is searchset)
   * @param { unsignedInt } total - The total number of matches
   * @returns { PackagingEngine } - this PackagingEngine
   */
  public setTotal(total: unsignedInt): PackagingEngine {
    this.bundle.total = total
    return this
  }

  /**
   * Add an entry to the package. The following resources are currently accepted:
   * Patient | Observation | Device | QuestionnaireResponse
   * @param { code } method - The HTTP-method to be used in this package. The method must be one of the following:
   * GET | POST | PUT | DELETE
   * @param { uri } fullUrl - The Absolute URL for the resource.
   * The fullUrl SHALL NOT disagree with the id in the resource -
   * i.e. if the fullUrl is not a urn:uuid, the URL shall be version-independent URL consistent with the Resource.id.
   * The fullUrl is a version independent reference to the resource
   * @param { Patient | Observation | Device | QuestionnaireResponse } resource - The resource the entry revolves around
   * @param { uri } url - The URL for this entry, relative to the root (the address to which the request is posted)
   * @param { string } ifNoneMatch - If the ETag values match, return a 304 Not Modified status
   * @param { string } ifModifiedSince - Only perform the operation if the last updated date matches
   * @param { string } ifMatch - Only perform the operation if the Etag value matches
   * @param { string } ifNoneExist -Instruct the server not to perform the create if a specified resource already exists.
   * (This is just the query portion of the URL - what follows the "?" (not including the "?").)
   */
  public addRequestEntry(entry: BundleEntry): PackagingEngine {
    if (!entry.resource || !entry.resource.resourceType) {
      throw new Error(
        'The provided entry contains no resource or the resource contains no resourceType'
      )
    }
    switch (entry.resource.resourceType) {
      case 'Patient':
        this.patientEntry = entry
        break
      case 'Observation':
        this.observationEntries = (this.observationEntries) ? this.observationEntries : [] as BundleEntry[]
        this.observationEntries.push(entry)
        break
      case 'Device':
        this.deviceEntries = (this.deviceEntries) ? this.deviceEntries : [] as BundleEntry[]
        this.deviceEntries.push(entry)
        break
      case 'QuestionnaireResponse':
        this.questionnaireResponseEntry = entry
        break
      default:
        throw new Error(
          'The provided resource is not part of the valid set of resources or does not have the resourceType specified.'
        )
    }
    if(entry.fullUrl){
      this.fullUrls.forEach(url => {
        if (entry.fullUrl === url){
          throw new Error(
            'The provided entry contain a fullUrl which is already present in the bundle.'
          )
        }
      })
      this.fullUrls.push(entry.fullUrl)
    }
    return this
  }

  /**
   * Packages the resources
   * @returns { Bundle } - the packaged resources
   */
  public package(): Bundle {
    if (!this.bundle.type) {
      throw new Error('The package must contain at least a type.')
    }
    this.setReferences()
    this.bundle.entry = []
    if (this.patientEntry) {
      this.bundle.entry.push(this.patientEntry)
    }
    if (this.questionnaireResponseEntry) {
      this.bundle.entry.push(this.questionnaireResponseEntry)
    }
    if (this.observationEntries.length > 0) {
      this.bundle.entry.push(...this.observationEntries)
    }
    if (this.deviceEntries.length > 0) {
      this.bundle.entry.push(...this.deviceEntries,)
    }
    this.bundle.entry = this.bundle.entry.filter(entry => !(!entry))
    return this.bundle
  }

  /**
   * Creates a bundle
   * @ignore
   */
  private make(): Bundle {
    const now: instant = new Date().toISOString().toString()
    const bundle: Bundle = {
      resourceType: 'Bundle',
      type: 'transaction',
      meta: {
        lastUpdated: now,
      },
      identifier: [
        {
          system: 'urn:ietf:rfc:3986',
          value: Guid.newGuid(),
        },
      ],
      timestamp: now,
    } as Bundle

    return bundle
  }

  private applyPatientReference(observation:Observation, patientReference: any): void {
    observation.subject = {
      reference: patientReference,
      type: 'Patient'
    } as Reference;
    observation.performer = [{
      reference: patientReference,
      type: 'Patient'
    } as Reference];
  }

  /**
   * Sets references from all entries to the patiententry, if one exists
   * @ignore
   */
  private setReferences(): void {
    if (!this.patientEntry) {
      return
    }
    let patientReference: string = ''
    // TODO: What is the system for the brugsaftale
    if (this.patientEntry !== {} && this.patientEntry.fullUrl) {
      patientReference = this.patientEntry.fullUrl
    }

    if (patientReference !== '' && this.observationEntries && this.observationEntries.length > 0) {
      this.observationEntries.forEach(entry => {

        const observation: Observation = entry.resource as Observation;
        this.applyPatientReference(observation, patientReference);

        if (observation.contained){
          observation.contained.forEach((c) => {
            this.applyPatientReference(c as Observation, patientReference);
          })
        }
      })
    }

    if (patientReference !== '' && this.questionnaireResponseEntry) {
      const questionnaireResponse = this.questionnaireResponseEntry
        .resource as QuestionnaireResponse
      questionnaireResponse.subject = questionnaireResponse.author = questionnaireResponse.source = {
        reference: patientReference,
        type: 'Patient'
      } as Reference
    }
  }
}
