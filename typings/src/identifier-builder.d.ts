import { Identifier, code, uri, dateTime } from 'fhir';
/** Class representing a FHIR Identifier builder */
export declare class IdentifierBuilder {
    /**
     * The Identifier that is being built
     * @private
     * @type { Identifier }
     * @ignore
     */
    private identifier;
    /**
     * Create a new FHIR ObservationBuilder
     */
    constructor();
    /**
     * Sets the use of the identifier
     * usual | official | temp | secondary (If known)
     * @param { code } [use] -  the use to set
     */
    setUse(use: code): IdentifierBuilder;
    /**
     * Sets the type of the identifier as a CodeableConcept
     * @param { string } [text] - Plain text representation of the concept
     * @param { uri } [system] - Identity of the terminology system
     * @param { string } [version] - Version of the system - if relevant
     * @param { code } [code] - Symbol in syntax defined by the system
     * @param { string } [display] - Representation defined by the system
     * @param { boolean } [userSelected] - If this coding was chosen directly by the user
     */
    setType(text?: string, system?: uri, version?: string, code?: code, display?: string, userSelected?: boolean): IdentifierBuilder;
    /**
     * Sets the namespace for the identifier value
     * @param { uri } system - the namespace to set
     */
    setSystem(system: uri): IdentifierBuilder;
    /**
     * Sets the (unique) value of the identifier
     * @param { string } value -  the value to set
     */
    setValue(value: string): IdentifierBuilder;
    /**
     * Sets the period wherin the period is in effect
     * @param { dateTime } [start] - The start of the period
     * @param { dateTime } [end] - The end of the period
     */
    setPeriod(start?: dateTime, end?: dateTime): IdentifierBuilder;
    /**
     * Sets the assigner of the identifier
     * @param { string } [reference] - Literal reference, Relative, internal or absolute URL
     * @param { Identifier } [identifier] - Logical reference, when literal reference is not known
     * @param { string } [display] - Text alternative for the resource
     */
    setAssigner(reference?: string, identifier?: Identifier, display?: string): IdentifierBuilder;
    /**
     * Returns the identifier
     * @returns { Identifier } the identifier
     */
    build(): Identifier;
}
