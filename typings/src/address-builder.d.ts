import { Address, code, dateTime } from 'fhir';
/** Class representing a FHIR Address builder */
export declare class AddressBuilder {
    /**
     * The address being built
     * @private
     * @type { Address }
     * @ignore
     */
    private address;
    /**
     * Creates a new FHIR AddressBuilder
     */
    constructor();
    /**
     * Sets the use of this address. The use must be within the following set of valid values:
     * home | work | temp | old
     * @param use - the use of the address.
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setUse(use: code): AddressBuilder;
    /**
     * Removes the use from the address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeUse(): boolean;
    /**
     * Sets the type of the address. The type must be within the following set:
     * postal | physical | both
     * @param { code }type the type of the address
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setType(type: code): AddressBuilder;
    /**
     * Removes the type of the address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeType(): boolean;
    /**
     * Sets a text representation of the address
     * @param { string } text - the text to be set
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setText(text: string): AddressBuilder;
    /**
     * Removes the text representation of the address from this address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeText(): boolean;
    /**
     * Returns all lines containing street name, number, direction & P.O. Box etc. for this address
     * @returns { string[] } - the lines
     */
    getLines(): string[];
    /**
     * Replaces the lines containing street name, number, direction & P.O. Box etc. for this address
     * with a new array containing a single line containing street name, number, direction & P.O. Box etc.
     * @param { string } line - the line containing street name, number, direction & P.O. Box etc. to be set
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setLines(line: string): AddressBuilder;
    /**
     * Appends a line containing street name, number, direction & P.O. Box etc. to the address
     * @param { string } line - the line containing street name, number, direction & P.O. Box etc. to be appended
     * @returns { AddressBuilder } - this AddressBuilder
     */
    addLine(line: string): AddressBuilder;
    /**
     * Removes one of the lines containing street name, number, direction & P.O. Box etc. if it exists
     * @param { string } line - line containing street name, number, direction & P.O. Box etc. to be removed
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeLine(line: string): boolean;
    /**
     * Sets the name of the city, town etc.
     * @param { string } city - the the name of the city, town etc. to be set
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setCity(city: string): AddressBuilder;
    /**
     * Removes the name of the city, town etc. from this address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeCity(): boolean;
    /**
     * Sets the sub-unit of country (abbreviations ok)
     * @param { string } district - the sub-unit of country to be set
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setDistrict(district: string): AddressBuilder;
    /**
     * Removes the sub-unit of country from this address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeDistrict(): boolean;
    /**
     * Sets the postal code for area
     * @param { string } state - the postal code for area
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setState(state: string): AddressBuilder;
    /**
     * Removes the postal code for area for this address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeState(): boolean;
    /**
     * Sets the country (e.g. can be ISO 3166 2 or 3 letter code)
     * @param { string } country - the country
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setCountry(country: string): AddressBuilder;
    /**
     * Removes the country for this address
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeCountry(): boolean;
    /**
     * Sets the time period when address was/is in use
     * If either start or end is left out, the period is considered open-ended.
     * Both should not be left out simoultaneously.
     * @param { dateTime } [start] - the start dateTime of the period
     * @param { dateTime } [end] - the end dateTime of the period
     * @returns { AddressBuilder } - this AddressBuilder
     */
    setPeriod(start: dateTime, end: dateTime): AddressBuilder;
    /**
     * Removes the time period when address was/is in use
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removePeriod(): boolean;
    /**
     * Builds and returns the address
     * @returns { Address } - the address
     */
    build(): Address;
    /**
     * Creates an Address
     * @ignore
     */
    private make;
}
