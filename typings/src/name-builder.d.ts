import { HumanName, code, dateTime } from 'fhir';
export declare class HumanNameBuilder {
    /**
     * The HumanName that is being built
     * @private
     * @type { HumanName }
     * @ignore
     */
    private name;
    /**
     * Creates a new FHIR HumanHumanNameBuilder
     */
    constructor();
    /**
     * Sets the use of this name. The use must be within the following set of valid values:
     * usual | official | temp | nickname | anonymous | old | maiden
     * @param use - the use of the name.
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setUse(use: code): HumanNameBuilder;
    /**
     * Removes the use from the name
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeUse(): boolean;
    /**
     * Sets text representation of the full name
     * @param text the text representation of the full name
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setText(text: string): HumanNameBuilder;
    /**
     * Removes the text representation of the full name
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeText(): boolean;
    /**
     * Sets the family name of this name
     * @param { string } family - the family name to be set
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setFamilyName(family: string): HumanNameBuilder;
    /**
     * Removes family name from this name
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeFamilyName(): boolean;
    /**
     * Returns all given names for patient
     * @returns { string[] } - the given names
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    getGivenNames(): string[];
    /**
     * Replaces the given name array with a new array containing a single given name
     * @param { string } name - the given name to be set
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setGivenName(name: string): HumanNameBuilder;
    /**
     * Appends an given name to the patient
     * @param { string } name - the given name to be appended
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    addGivenName(name: string): HumanNameBuilder;
    /**
     * Removes one of the given names if it exists
     * @param { string } name - the name to remove
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeGivenName(name: string): boolean;
    /**
     * Returns all prefixes for patient
     * @returns { string[] } - the prefixes
     */
    getPrefixes(): string[];
    /**
     * Replaces the prefix array with a new array containing a single prefix
     * @param { string } prefix - the prefix to be set
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setPrefixes(prefix: string): HumanNameBuilder;
    /**
     * Appends an prefix to the name
     * @param { string } prefix - the prefix to be appended
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    addPrefix(prefix: string): HumanNameBuilder;
    /**
     * Removes one of the given names if it exists
     * @param { string } name - the name to remove
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removePrefix(prefix: string): boolean;
    /**
     * Returns all suffixes for patient
     * @returns { string[] } - the suffixes
     */
    getSuffixes(): string[];
    /**
     * Replaces the suffix array with a new array containing a single suffix
     * @param { string } suffix - the suffix to be set
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setSuffixes(suffix: string): HumanNameBuilder;
    /**
     * Appends an suffix to the name
     * @param { string } suffix - the suffix to be appended
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    addSuffix(suffix: string): HumanNameBuilder;
    /**
     * Removes one of the given names if it exists
     * @param { string } name - the name to remove
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removeSuffix(suffix: string): boolean;
    /**
     * Sets the time period when name was/is in use
     * If either start or end is left out, the period is considered open-ended.
     * Both should not be left out simoultaneously.
     * @param { dateTime } [start] - the start dateTime of the period
     * @param { dateTime } [end] - the end dateTime of the period
     * @returns { HumanNameBuilder } - this HumanNameBuilder
     */
    setPeriod(start: dateTime, end: dateTime): HumanNameBuilder;
    /**
     * Removes the time period when name was/is in use
     * @returns { boolean } - boolean indicating whether the operation was successful
     */
    removePeriod(): boolean;
    /**
     * Builds and returns the HumanName
     * @returns { HumanName } - the HumanName
     */
    build(): HumanName;
    /**
     * Creates a HumanName
     * @ignore
     */
    private make;
}
